/// Wrapper around the key we are using to prove who we are
pub struct ForwardKey {
    pub key: String,
}
