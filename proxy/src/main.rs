#[macro_use]
extern crate log;

mod config;
mod key;
mod proxy;

use actix_web::{middleware, App, HttpServer};
use envconfig::Envconfig;
use url::Url;

fn main() -> std::io::Result<()> {
    // We can safely ignore if the .env file is missing
    let _ = dotenv::dotenv();
    env_logger::init();

    let config = config::Config::init().unwrap();
    let forward_url = config.forward_url;
    let forward_key = config.forward_key;

    info!("Starting proxy...");
    HttpServer::new(move || {
        App::new()
            .wrap(middleware::Logger::default())
            .data(Url::parse(&forward_url).unwrap())
            .data(key::ForwardKey {
                key: forward_key.clone(),
            })
            .configure(proxy::config)
    })
    .bind(format!("0.0.0.0:{}", config.port))?
    .run()
}
