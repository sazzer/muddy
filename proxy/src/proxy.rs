use crate::key::ForwardKey;
use actix_web::{client::Client, web, Error, HttpRequest, HttpResponse};
use futures::Future;
use url::Url;

/// Actix Web async handler for proxying requests to the downstream service
fn proxy_request(
    req: HttpRequest,
    payload: web::Payload,
    url: web::Data<Url>,
    client: web::Data<Client>,
    forward_key: web::Data<ForwardKey>,
) -> impl Future<Item = HttpResponse, Error = Error> {
    // Determine the URL Path that the request was for.
    let mut requested_path = req.path().to_owned();
    if requested_path.starts_with('/') {
        requested_path.remove(0);
    }
    // Determine the base URL path of our target
    let base_path = url.path();

    // Then combine them together
    let new_path = format!("{}{}", base_path, requested_path);

    // Now build the complete URL that we are forwarding the request to
    let mut new_url = url.get_ref().clone();
    new_url.set_path(&new_path);
    new_url.set_query(req.uri().query());

    // Build the request that we are going to make to the server we are proxying
    let mut forwarded_req = client
        .request_from(new_url.as_str(), req.head())
        .no_decompress()
        .header("x-muddy-key", forward_key.key.to_string());
    forwarded_req.headers_mut().remove("host");

    let forwarded_req = if let Some(addr) = req.head().peer_addr {
        forwarded_req.header("x-forwarded-for", format!("{}", addr.ip()))
    } else {
        forwarded_req
    };
    debug!("Making request: {:?}", forwarded_req);

    // And then actually make the request
    forwarded_req
        .send_stream(payload)
        .map_err(Error::from)
        .map(|res| {
            // When we get a response to our request we want to munge it a little first
            let mut client_resp = HttpResponse::build(res.status());
            for (header_name, header_value) in res
                .headers()
                .iter()
                // Strip out a few specific header values
                .filter(|(h, _)| *h != "connection" && *h != "server")
            {
                client_resp.header(header_name.clone(), header_value.clone());
            }

            // Add in some headers of our own
            client_resp.header("server", "muddy-proxy");

            // And then stream the response back to our client
            client_resp.streaming(res)
        })
}

/// Configuration handler for the Actix Web service to proxy everything on "/api/**" and send it to the
/// downstream service
pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/api/**")
            .data(Client::new())
            .route(web::to_async(proxy_request)),
    );
}
