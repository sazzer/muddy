use envconfig::*;
use envconfig_derive::*;

/// Configuration for the Proxy Server
#[derive(Envconfig, Debug)]
pub struct Config {
    /// The port to listen on
    #[envconfig(from = "PORT", default = "8000")]
    pub port: u16,

    /// The URL of the server that we are proxying
    #[envconfig(from = "FORWARD_URL")]
    pub forward_url: String,

    /// The secure key to include in forwarded requests to prove who we are
    #[envconfig(from = "MUDDY_KEY")]
    pub forward_key: String,
}
