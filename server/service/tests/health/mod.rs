use crate::{assertions::*, TestService};
use rocket::http::Status;
use serde_json::json;

#[test]
fn test_healthchecks() {
    let service = TestService::default();

    let mut response = service.get_client().get("/health").dispatch();
    assert_response_status(&response, Status::Ok);
    assert_json_body(
        &mut response,
        json!({
            "components": {
                "database": {
                    "healthy": true,
                    "message": "Database Connection OK"
                },
                "redis": {
                    "healthy": true,
                    "message": "Redis Connection OK"
                }
            },
            "healthy": true
        }),
    );
}
