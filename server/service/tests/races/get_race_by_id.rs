use crate::{assertions::*, TestService};
use muddy_testdata::races::RaceSeed;
use rocket::http::Status;
use serde_json::json;

#[test]
fn test_get_race_by_invalid_id() {
    let service = TestService::default();

    let mut response = service.get_client().get("/api/races/invalidId").dispatch();
    assert_response_status(&response, Status::NotFound);
    assert_json_body(
        &mut response,
        json!({
            "type": "tag:muddy,2019:races/problems/not_found",
            "title": "The requested race was not found",
            "status": 404,
        }),
    );
}

#[test]
fn test_get_race_by_unknown_id() {
    let service = TestService::default();

    let mut response = service
        .get_client()
        .get("/api/races/ffc1c729-2f12-44e2-8aa8-1517f80927c3")
        .dispatch();
    assert_response_status(&response, Status::NotFound);
    assert_json_body(
        &mut response,
        json!({
            "type": "tag:muddy,2019:races/problems/not_found",
            "title": "The requested race was not found",
            "status": 404,
        }),
    );
}

#[test]
fn test_get_race_by_known_id() {
    let service = TestService::default();

    let race = RaceSeed {
        name: "Human".to_owned(),
        description: "Human Beings are everywhere".to_owned(),
        id: uuid::Uuid::parse_str("ffc1c729-2f12-44e2-8aa8-1517f80927c3").unwrap(),
        ..Default::default()
    };
    service.seed(&race);

    let mut response = service
        .get_client()
        .get("/api/races/ffc1c729-2f12-44e2-8aa8-1517f80927c3")
        .dispatch();
    assert_response_status(&response, Status::Ok);
    assert_json_body(
        &mut response,
        json!({
            "id": "ffc1c729-2f12-44e2-8aa8-1517f80927c3",
            "name": "Human",
            "description": "Human Beings are everywhere",
        }),
    );
}
