use lazy_static::lazy_static;
use log::info;
use muddy_testdata::seeddata::SeedData;
use postgres::{Connection, TlsMode};
use rocket::local::Client;
use testcontainers::{
    clients::Cli,
    images::{postgres::Postgres, redis::Redis},
    Container, Docker,
};

lazy_static! {
    static ref DOCKER: Cli = { Cli::default() };
}

/// The wrapper around the service under test
pub struct TestService {
    client: Client,
    postgres_uri: String,
    #[allow(dead_code)]
    postgres: Container<'static, Cli, Postgres>,
    #[allow(dead_code)]
    redis: Container<'static, Cli, Redis>,
}

impl Default for TestService {
    /// Create the service that we are testing
    fn default() -> TestService {
        let _ = env_logger::try_init();
        info!("Creating service to test");

        let host = std::env::var("RUST_DOCKER_HOST").unwrap_or_else(|_| "localhost".to_owned());

        let redis = DOCKER.run(Redis::default().with_tag("4.0.14-alpine"));
        let redis_uri = format!("redis://{}:{}", host, redis.get_host_port(6379).unwrap());

        let postgres = DOCKER.run(Postgres::default());
        let postgres_uri = format!(
            "postgres://postgres@{}:{}",
            host,
            postgres.get_host_port(5432).unwrap()
        );

        let config = muddy_server::config::Config {
            database: muddy_server::config::database::Config {
                url: postgres_uri.to_owned(),
            },
            redis: muddy_server::config::redis::Config {
                url: redis_uri.to_owned(),
            },
            web: muddy_server::config::web::Config { port: 0 },
        };
        let service = muddy_server::build_server(config);
        let client = Client::new(service).expect("valid rocket instance");

        TestService {
            client,
            postgres_uri,
            postgres,
            redis,
        }
    }
}

impl TestService {
    /// Get a Client that can be used to test the Rocket service
    pub fn get_client(&self) -> &Client {
        &self.client
    }

    // Seed the database with a record represented by the provided Seed Data
    pub fn seed(&self, data: &dyn SeedData) {
        let conn = Connection::connect(self.postgres_uri.clone(), TlsMode::None).unwrap();
        conn.execute(&data.query(), &data.binds()).unwrap();
    }
}
