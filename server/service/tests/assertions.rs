use galvanic_assert::{matchers::*, *};
use rocket::http::Status;
use rocket::Response;
use serde_json::{from_str, Value};

/// Assert that the status code in the given response matches the one expected
pub fn assert_response_status<'r>(response: &Response<'r>, expected_status: Status) {
    assert_that!(&response.status(), eq(expected_status));
}

pub fn assert_json_body<'r>(response: &mut Response<'r>, expected_body: Value) {
    let actual_body: Value = from_str(&response.body_string().unwrap()).unwrap();
    assert_that!(&actual_body, eq(expected_body));
}
