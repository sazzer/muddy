CREATE TABLE races(
    race_id UUID PRIMARY KEY,
    version UUID NOT NULL,
    created TIMESTAMPTZ NOT NULL,
    updated TIMESTAMPTZ NOT NULL,
    name TEXT,
    description TEXT
);
