use rocket::http::Status;
use serde::Serialize;

/// API Representation of a Problem response
#[derive(Serialize, Debug)]
pub struct Problem {
    #[serde(skip_serializing_if = "Option::is_none")]
    r#type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    title: Option<String>,
    status: u16,
    #[serde(skip_serializing_if = "Option::is_none")]
    detail: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    instance: Option<String>,
}

impl Default for Problem {
    fn default() -> Self {
        Problem {
            r#type: None,
            title: None,
            status: 400,
            detail: None,
            instance: None,
        }
    }
}

#[allow(dead_code)]
impl Problem {
    /// Update the value of the "type" field on the Problem
    pub fn with_type(mut self, r#type: &str) -> Self {
        self.r#type = Some(r#type.to_owned());
        self
    }

    /// Update the value of the "title" field on the Problem
    pub fn with_title(mut self, title: &str) -> Self {
        self.title = Some(title.to_owned());
        self
    }

    /// Update the value of the "detail" field on the Problem
    pub fn with_detail(mut self, detail: &str) -> Self {
        self.detail = Some(detail.to_owned());
        self
    }

    /// Update the value of the "instance" field on the Problem
    pub fn with_instance(mut self, instance: &str) -> Self {
        self.instance = Some(instance.to_owned());
        self
    }

    /// Update the value of the "status" field on the Problem
    pub fn with_status(mut self, status: Status) -> Self {
        self.status = status.code;
        self
    }
}
