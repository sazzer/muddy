use chrono::{DateTime, Utc};
use uuid::Uuid;

/// Structure representing the raw Identity of some model in the underlying system
#[derive(Debug, PartialEq)]
pub struct Identity<ID> {
    pub id: ID,
    pub version: Uuid,
    pub created: DateTime<Utc>,
    pub updated: DateTime<Utc>,
}

/// Structure representing a persisted Model in the underlying system.
/// This consists of the Identity of the model and the data that makes it up
#[derive(Debug, PartialEq)]
pub struct Model<ID, DATA> {
    pub identity: Identity<ID>,
    pub data: DATA,
}
