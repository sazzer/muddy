use super::DatabaseWrapper;
use crate::health::Healthcheck;

impl Healthcheck for DatabaseWrapper {
    /// Check the health of the Database connection.
    /// This effectively just does a "SELECT 1" on the Database connection, and returns whether that was a success or not
    fn check_health(&self) -> Result<String, String> {
        let client = self.get_connection();
        match client {
            Ok(conn) => {
                let result = conn.execute("SELECT 1", &[]);
                match result {
                    Ok(_) => Ok("Database Connection OK".to_owned()),
                    Err(err) => Err(format!("Error executing SQL: {}", err)),
                }
            }
            Err(err) => Err(format!("Error getting connection: {}", err)),
        }
    }
}

#[cfg(test)]
mod test {
    use super::super::test::TestDatabase;
    use super::*;
    use galvanic_assert::{matchers::*, *};

    #[test]
    fn test_healthcheck() {
        let _ = env_logger::try_init();
        let db = TestDatabase::new();

        let health = db.get_wrapper().check_health();

        assert_that!(
            &health,
            variant::maybe_ok(eq("Database Connection OK".to_owned()))
        );
    }
}
