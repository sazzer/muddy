use super::DatabaseWrapper;
use postgres::{rows::Row, types::ToSql};
use std::error::Error;

/// Enumeration of the errors that can occur with the database
#[derive(Debug)]
pub enum DatabaseError<E> {
    ConnectionError,
    QueryError,
    NoRowsError,
    MultipleRowsError,
    MappingError(E),
}

/// Trait that represents the database connection and how we can interact with it
pub trait QueryableDatabase {
    /// Query the database for a single object
    fn query_for_object<T, F, E>(
        &self,
        query: &str,
        params: &[&dyn ToSql],
        mapper: F,
    ) -> Result<T, DatabaseError<E>>
    where
        F: Fn(Row) -> Result<T, E>,
        E: Error;

    /// Perform a simple update on the database
    fn update(&self, query: &str, params: &[&dyn ToSql]) -> Result<u64, DatabaseError<()>>;
}

impl QueryableDatabase for DatabaseWrapper {
    /// Query the database for a single object
    fn query_for_object<T, F, E>(
        &self,
        query: &str,
        params: &[&dyn ToSql],
        mapper: F,
    ) -> Result<T, DatabaseError<E>>
    where
        F: Fn(Row) -> Result<T, E>,
        E: Error,
    {
        match self.get_connection() {
            Ok(conn) => match conn.query(query, params) {
                Ok(results) => {
                    if results.is_empty() {
                        info!("Query {} with params {:?} returned no rows", query, params);
                        Err(DatabaseError::NoRowsError)
                    } else if results.len() > 1 {
                        info!(
                            "Query {} with params {:?} returned multiple rows: {}",
                            query,
                            params,
                            results.len()
                        );
                        Err(DatabaseError::MultipleRowsError)
                    } else {
                        let row = results.get(0);
                        let mapped = mapper(row);
                        mapped.map_err(DatabaseError::MappingError)
                    }
                }
                Err(e) => {
                    error!(
                        "Failed to execute query {} with params {:?}: {}",
                        query, params, e
                    );
                    Err(DatabaseError::QueryError)
                }
            },
            Err(e) => {
                error!("Failed to get connection: {}", e);
                Err(DatabaseError::ConnectionError)
            }
        }
    }

    /// Perform a simple update on the database
    fn update(&self, query: &str, params: &[&dyn ToSql]) -> Result<u64, DatabaseError<()>> {
        match self.get_connection() {
            Ok(conn) => match conn.execute(query, params) {
                Ok(updated) => {
                    info!("Number of rows updated: {}", updated);
                    Ok(updated)
                }
                Err(e) => {
                    error!(
                        "Failed to execute query {} with params {:?}: {}",
                        query, params, e
                    );
                    Err(DatabaseError::QueryError)
                }
            },
            Err(e) => {
                error!("Failed to get connection: {}", e);
                Err(DatabaseError::ConnectionError)
            }
        }
    }
}
