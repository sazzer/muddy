use super::DatabaseWrapper;
use std::sync::Arc;

/// Module for managing the connection to the database
pub struct DatabaseModule {
    database_wrapper: Arc<DatabaseWrapper>,
}

impl DatabaseModule {
    /// Create a new instance of the Database module
    pub fn new(database_url: &str) -> Result<Self, &str> {
        let wrapper = DatabaseWrapper::new(database_url)?;

        Ok(DatabaseModule {
            database_wrapper: Arc::new(wrapper),
        })
    }

    /// Get the wrapper used to communicate with Database
    pub fn get_database_wrapper(&self) -> Arc<DatabaseWrapper> {
        self.database_wrapper.clone()
    }
}
