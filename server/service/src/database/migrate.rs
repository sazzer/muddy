use dbmigrate_lib::{get_driver, read_migration_files};
use std::path::Path;

/// Migrate the database to the latest schema
pub fn migrate(url: &str) -> Result<(), &str> {
    let from = Path::new("./migrations");
    info!("Migrating database schema from {:?}", from);

    let migration_files = read_migration_files(from).or(Err("Failed to read migration files"))?;
    let driver = get_driver(url).or(Err("Failed to get database driver"))?;
    let current = driver.get_current_number();
    let max = migration_files.keys().max().unwrap_or(&0);

    if current == *max {
        info!("Schema is up-to-date");
    } else {
        for (number, migration) in migration_files.iter() {
            if *number > current {
                let mig_file = migration.up.as_ref().unwrap();
                debug!("Applying migration #{}: {}", mig_file.number, mig_file.name);

                let contents = mig_file
                    .content
                    .clone()
                    .ok_or("Failed to load migration file")?;

                driver
                    .migrate(contents, mig_file.number)
                    .or(Err("Failed to apply migration file"))?;
            }
        }
    }

    Ok(())
}
