use super::{DatabaseWrapper, QueryableDatabase};
use lazy_static::lazy_static;
use muddy_testdata::seeddata::SeedData;
use std::sync::Arc;
use testcontainers::{clients::Cli, images::postgres::Postgres, Container, Docker};

lazy_static! {
    static ref DOCKER: Cli = { Cli::default() };
}

pub struct TestDatabase {
    #[allow(dead_code)]
    container: Container<'static, Cli, Postgres>,
    wrapper: Arc<DatabaseWrapper>,
}

impl<'d> TestDatabase {
    pub fn new() -> Self {
        let container = DOCKER.run(Postgres::default());

        let host = std::env::var("RUST_DOCKER_HOST").unwrap_or_else(|_| "localhost".to_owned());
        let host_port = container.get_host_port(5432).unwrap();
        let url = format!("postgres://postgres@{}:{}", host, host_port);

        let wrapper = DatabaseWrapper::new(&url).expect("Create Database Wrapper");

        TestDatabase {
            container,
            wrapper: Arc::new(wrapper),
        }
    }

    pub fn get_wrapper(&self) -> Arc<DatabaseWrapper> {
        self.wrapper.clone()
    }

    pub fn seed(&self, data: &dyn SeedData) {
        info!(
            "Seeding database with query {} and binds {:?}",
            data.query(),
            data.binds()
        );
        self.wrapper
            .update(&data.query(), &data.binds()[..])
            .unwrap();
    }
}
