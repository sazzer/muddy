use super::migrate::migrate;
use postgres::params::*;
use r2d2_postgres::{PostgresConnectionManager, TlsMode};

/// Wrapper around the Database Connection
pub struct DatabaseWrapper {
    pool: r2d2::Pool<PostgresConnectionManager>,
    pub url: String,
}

impl DatabaseWrapper {
    /// Create a new instance of the Database wrapper
    pub fn new(url: &str) -> Result<Self, &str> {
        let connect_params = url.into_connect_params().or(Err("Invalid Database URL"))?;
        debug!("Connecting to database: {:?}", connect_params);

        let manager = PostgresConnectionManager::new(connect_params, TlsMode::None)
            .or(Err("Postgres Connection Failed"))?;
        let pool = r2d2::Pool::new(manager).or(Err("Connection Pool Construction Failed"))?;

        migrate(url)?;
        info!("Created database connection: {}", url);
        Ok(DatabaseWrapper {
            pool,
            url: url.to_owned(),
        })
    }

    /// Get a connection to the database that we can use for queries
    pub fn get_connection(
        &self,
    ) -> Result<r2d2::PooledConnection<PostgresConnectionManager>, r2d2::Error> {
        self.pool.get()
    }
}
