use uuid::Uuid;

/// Type representing the ID of a Race
pub type RaceId = Uuid;

/// Type representing the data of a Race within the service
#[derive(Debug, PartialEq)]
pub struct RaceData {
    pub name: String,
    pub description: String,
}
