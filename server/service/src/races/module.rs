use super::dao::RacesDao;
use super::http;
use crate::database::DatabaseModule;
use crate::web::WebConfigurable;
use rocket::{routes, Rocket};
use std::sync::Arc;

/// Module for managing Races
pub struct RacesModule {
    dao: Arc<RacesDao>,
}

impl RacesModule {
    /// Construct a new instance of the Races Module
    pub fn new(db: &DatabaseModule) -> Self {
        RacesModule {
            dao: Arc::new(RacesDao::new(db.get_database_wrapper())),
        }
    }
}

impl WebConfigurable for RacesModule {
    /// Configure the web server to provide the races routes
    fn configure_web_server(&self, rocket: Rocket) -> Rocket {
        rocket
            .manage(self.dao.clone())
            .manage(self.dao.clone() as Arc<dyn super::RaceRetriever>)
            .mount("/", routes![http::get_race_by_id])
    }
}
