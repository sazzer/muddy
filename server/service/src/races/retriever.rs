use super::{RaceData, RaceId};
use crate::model::*;
use mockall::*;

/// Trait describing how to retrieve races
#[automock]
pub trait RaceRetriever: Sync + Send {
    /// Retrieve a race by the given ID
    fn get_race_by_id(&self, id: RaceId) -> Option<Model<RaceId, RaceData>>;
}
