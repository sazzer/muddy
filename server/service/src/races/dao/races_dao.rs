use crate::database::DatabaseWrapper;
use std::sync::Arc;

/// DAO that we can use to interact with Races in the database
pub struct RacesDao {
    pub db: Arc<DatabaseWrapper>,
}

impl RacesDao {
    /// Create a new instance of the Races DAO
    pub fn new(db: Arc<DatabaseWrapper>) -> Self {
        RacesDao { db }
    }
}
