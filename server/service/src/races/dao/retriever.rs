use super::super::{RaceData, RaceId, RaceRetriever};
use super::RacesDao;
use crate::database::QueryableDatabase;
use crate::model::*;
use uuid::Uuid;

/// Implementation of the RaceRetriever trait backed by the Races DAO
/// This will allow us to retrieve race details from the database
impl RaceRetriever for RacesDao {
    /// Retrieve a race by the given ID
    fn get_race_by_id(&self, id: RaceId) -> Option<Model<RaceId, RaceData>> {
        match self.db.query_for_object(
            "SELECT * FROM races WHERE race_id = $1",
            &[&id],
            map_row_to_race,
        ) {
            Ok(race) => Some(race),
            Err(e) => {
                info!("Failed to file a race with ID {}: {:?}", id, e);
                None
            }
        }
    }
}

#[derive(Debug)]
struct RaceParseError {}

impl std::fmt::Display for RaceParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Failed to parse Race record")
    }
}

impl std::error::Error for RaceParseError {}
/// Map a single row from the database into a Race model
fn map_row_to_race(row: postgres::rows::Row) -> Result<Model<RaceId, RaceData>, RaceParseError> {
    Ok(Model {
        identity: Identity {
            id: row.get::<&str, Uuid>("race_id"),
            version: row.get::<&str, Uuid>("version"),
            created: row.get("created"),
            updated: row.get("updated"),
        },
        data: RaceData {
            name: row.get("name"),
            description: row.get("description"),
        },
    })
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::database::test::TestDatabase;
    use galvanic_assert::{matchers::*, *};
    use muddy_testdata::races::RaceSeed;

    #[test]
    fn test_get_unknown_race_by_id() {
        let _ = env_logger::try_init();
        let db = TestDatabase::new();

        let dao = RacesDao::new(db.get_wrapper());

        let race =
            dao.get_race_by_id(Uuid::parse_str("0D9CD68B-2289-451A-891C-D22C11119B0F").unwrap());

        assert_that!(&race, eq(None));
    }

    #[test]
    fn test_get_known_race_by_id() {
        let _ = env_logger::try_init();
        let db = TestDatabase::new();

        let seeded_race = RaceSeed {
            ..Default::default()
        };
        db.seed(&seeded_race);

        let dao = RacesDao::new(db.get_wrapper());

        let race = dao.get_race_by_id(seeded_race.id);

        assert_that!(
            &race,
            variant::maybe_some(eq(Model {
                identity: Identity {
                    id: seeded_race.id,
                    version: seeded_race.version,
                    created: seeded_race.created,
                    updated: seeded_race.updated,
                },
                data: RaceData {
                    name: seeded_race.name,
                    description: seeded_race.description,
                },
            }))
        );
    }
}
