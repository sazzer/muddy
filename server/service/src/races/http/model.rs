use serde::Serialize;

/// API Representation of a single Race for a character
#[derive(Serialize)]
pub struct Race {
    id: String,
    name: String,
    description: String,
}

impl Race {
    pub fn new(id: String, name: String, description: String) -> Self {
        Race {
            id,
            name,
            description,
        }
    }
}
