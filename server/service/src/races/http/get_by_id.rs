use super::super::RaceRetriever;
use super::Race as RaceModel;
use crate::http::problem::Problem;
use rocket::{get, http::Status, response::status::Custom, State};
use rocket_contrib::json::Json;
use std::sync::Arc;
use uuid::Uuid;

/// The handler that will get a single Race record by ID
#[get("/api/races/<id>", format = "json")]
pub fn get_race_by_id(
    id: String,
    race_retriever: State<Arc<dyn RaceRetriever>>,
) -> Custom<Result<Json<RaceModel>, Json<Problem>>> {
    match Uuid::parse_str(&id) {
        Ok(race_id) => {
            let race = race_retriever.get_race_by_id(race_id);
            info!("Found race {:?} for ID {}", race, id);

            match race {
                Some(race_model) => Custom(
                    Status::Ok,
                    Ok(Json(RaceModel::new(
                        format!("{}", race_model.identity.id),
                        race_model.data.name,
                        race_model.data.description,
                    ))),
                ),
                None => Custom(
                    Status::NotFound,
                    Err(Json(
                        Problem::default()
                            .with_type("tag:muddy,2019:races/problems/not_found")
                            .with_title("The requested race was not found")
                            .with_status(Status::NotFound),
                    )),
                ),
            }
        }
        Err(e) => {
            warn!("Requested Race ID was not a valid UUID: {}", e);
            Custom(
                Status::NotFound,
                Err(Json(
                    Problem::default()
                        .with_type("tag:muddy,2019:races/problems/not_found")
                        .with_title("The requested race was not found")
                        .with_status(Status::NotFound),
                )),
            )
        }
    }
}

#[cfg(test)]
mod test {
    use super::super::super::retriever::MockRaceRetriever;
    use super::*;
    use crate::model::*;
    use crate::races::RaceData;
    use chrono::Utc;
    use galvanic_assert::{matchers::*, *};
    use rocket::{local::Client, routes};
    use serde_json::{from_str, json, Value};

    fn test_get_race_by_id(
        race_id: &str,
        race_retriever: Arc<dyn super::RaceRetriever>,
        expected_status_code: Status,
        expected_body: Value,
    ) {
        let rocket = rocket::ignite()
            .manage(race_retriever)
            .mount("/", routes![get_race_by_id]);
        let client = Client::new(rocket).expect("valid rocket instance");

        let url = format!("/api/races/{}", race_id);
        let mut response = client.get(url).dispatch();
        assert_that!(&response.status(), eq(expected_status_code));

        let actual_body: Value = from_str(&response.body_string().unwrap()).unwrap();
        assert_that!(&actual_body, eq(expected_body));
    }

    #[test]
    fn test_get_race_by_id_invalid() {
        let mock = MockRaceRetriever::new();

        let expected_status_code = Status::NotFound;
        let expected_body = json!({
            "type": "tag:muddy,2019:races/problems/not_found",
            "title": "The requested race was not found",
            "status": 404,
        });

        test_get_race_by_id(
            "invalidId",
            Arc::new(mock),
            expected_status_code,
            expected_body,
        );
    }

    #[test]
    fn test_get_race_by_id_unknown() {
        let mut mock = MockRaceRetriever::new();
        mock.expect_get_race_by_id()
            .with(mockall::predicate::eq(
                uuid::Uuid::parse_str("715d0523-3bc5-4324-bee7-5b31cbd3747f").unwrap(),
            ))
            .times(1)
            .returning(|_| None);

        let expected_status_code = Status::NotFound;
        let expected_body = json!({
            "type": "tag:muddy,2019:races/problems/not_found",
            "title": "The requested race was not found",
            "status": 404,
        });

        test_get_race_by_id(
            "715d0523-3bc5-4324-bee7-5b31cbd3747f",
            Arc::new(mock),
            expected_status_code,
            expected_body,
        );
    }

    #[test]
    fn test_get_race_by_id_known() {
        let mut mock = MockRaceRetriever::new();
        mock.expect_get_race_by_id()
            .with(mockall::predicate::eq(
                uuid::Uuid::parse_str("715d0523-3bc5-4324-bee7-5b31cbd3747f").unwrap(),
            ))
            .times(1)
            .returning(|id| {
                Some(Model {
                    identity: Identity {
                        id,
                        version: uuid::Uuid::new_v4(),
                        created: Utc::now(),
                        updated: Utc::now(),
                    },
                    data: RaceData {
                        name: "Test Race".to_owned(),
                        description: "This is a test race".to_owned(),
                    },
                })
            });

        let expected_status_code = Status::Ok;
        let expected_body = json!({
            "id": "715d0523-3bc5-4324-bee7-5b31cbd3747f",
            "name": "Test Race",
            "description": "This is a test race",
        });

        test_get_race_by_id(
            "715d0523-3bc5-4324-bee7-5b31cbd3747f",
            Arc::new(mock),
            expected_status_code,
            expected_body,
        );
    }
}
