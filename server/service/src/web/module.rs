use super::WebConfigurable;
use rocket::{
    config::{Config, ConfigError, Environment},
    Rocket,
};

/// Module to represent the Web Server
pub struct WebModule<'a> {
    port: u16,
    modules: Vec<&'a dyn WebConfigurable>,
}

impl<'a> WebModule<'a> {
    /// Construct a new instance of the Web Module
    pub fn new(port: u16, modules: Vec<&'a dyn WebConfigurable>) -> Self {
        WebModule { port, modules }
    }

    /// Actually build a server for us to work with
    pub fn build_server(&self) -> Result<Rocket, ConfigError> {
        let config = Config::build(Environment::active()?)
            .port(self.port)
            .finalize()?;

        let mut server = rocket::custom(config);

        for module in &self.modules {
            server = module.configure_web_server(server);
        }

        Ok(server)
    }
}
