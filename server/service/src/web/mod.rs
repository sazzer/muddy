mod module;
mod web_configurable;

pub use module::WebModule;
pub use web_configurable::WebConfigurable;
