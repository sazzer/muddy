use rocket::Rocket;

/// Trait for modules that can contribute to the webapp configuration
pub trait WebConfigurable {
    fn configure_web_server(&self, rocket: Rocket) -> Rocket;
}
