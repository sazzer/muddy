use super::RedisWrapper;
use std::sync::Arc;

/// Module for managing the connection to Redis
pub struct RedisModule {
    redis_wrapper: Arc<RedisWrapper>,
}

impl RedisModule {
    /// Create a new instance of the Redis module
    pub fn new(redis_url: &str) -> Result<Self, redis::RedisError> {
        RedisWrapper::new(redis_url)
            .map(Arc::new)
            .map(|wrapper| RedisModule {
                redis_wrapper: wrapper,
            })
    }

    /// Get the wrapper used to communicate with Redis
    pub fn get_redis_wrapper(&self) -> Arc<RedisWrapper> {
        self.redis_wrapper.clone()
    }
}
