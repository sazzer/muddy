use super::RedisWrapper;
use lazy_static::lazy_static;
use std::rc::Rc;
use testcontainers::{clients::Cli, images::redis::Redis, Container, Docker};

lazy_static! {
    static ref DOCKER: Cli = { Cli::default() };
}

pub struct TestRedis {
    #[allow(dead_code)]
    container: Container<'static, Cli, Redis>,
    wrapper: Rc<RedisWrapper>,
}

impl TestRedis {
    pub fn new() -> Self {
        let container = DOCKER.run(Redis::default().with_tag("4.0.14-alpine"));

        let host = std::env::var("RUST_DOCKER_HOST").unwrap_or_else(|_| "localhost".to_owned());
        let host_port = container.get_host_port(6379).unwrap();
        let url = format!("redis://{}:{}", host, host_port);

        let wrapper = RedisWrapper::new(&url).unwrap();

        TestRedis {
            container,
            wrapper: Rc::new(wrapper),
        }
    }

    pub fn get_wrapper(&self) -> Rc<RedisWrapper> {
        self.wrapper.clone()
    }
}
