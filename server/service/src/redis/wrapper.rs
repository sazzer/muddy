use redis::{Client, Connection, RedisError};

/// Wrapper around the Redis connection that we can use for our own code
pub struct RedisWrapper {
    client: Client,
}

impl RedisWrapper {
    /// Create a new instance of the Redis wrapper
    pub fn new(redis_url: &str) -> Result<Self, redis::RedisError> {
        Client::open(redis_url).map(|client| RedisWrapper { client })
    }

    /// Get a new connection to the Redis server
    pub fn get_connection(&self) -> Result<Connection, RedisError> {
        self.client.get_connection()
    }
}
