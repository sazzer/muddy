use super::RedisWrapper;
use crate::health::Healthcheck;

impl Healthcheck for RedisWrapper {
    /// Check the health of the Redis connection.
    /// This effectively just does a PING on the Redis connection, and returns whether that was a success or not
    fn check_health(&self) -> Result<String, String> {
        match self.get_connection() {
            Ok(mut connection) => {
                let result: redis::RedisResult<()> = redis::cmd("PING").query(&mut connection);
                debug!("Ping result: {:?}", result);

                match result {
                    Ok(_) => Ok("Redis Connection OK".to_owned()),
                    Err(err) => Err(format!("Error pinging redis: {}", err)),
                }
            }
            Err(err) => Err(format!("Error connecting to redis: {}", err)),
        }
    }
}

#[cfg(test)]
mod test {
    use super::super::test::TestRedis;
    use super::*;
    use galvanic_assert::{matchers::*, *};

    #[test]
    fn test_healthcheck() {
        let _ = env_logger::try_init();
        let redis = TestRedis::new();

        let wrapper = redis.get_wrapper();

        let health = wrapper.check_health();

        assert_that!(
            &health,
            variant::maybe_ok(eq("Redis Connection OK".to_owned()))
        );
    }
}
