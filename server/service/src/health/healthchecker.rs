use super::Healthcheck;
use std::collections::HashMap;
use std::sync::Arc;

/// Struct that can check the health of the entire system
pub struct Healthchecker {
    /// The components that can be checked
    components: HashMap<String, Arc<dyn Healthcheck>>,
}

impl Healthchecker {
    /// Construct a new health checker
    pub fn new(components: HashMap<String, Arc<dyn Healthcheck>>) -> Self {
        Healthchecker { components }
    }

    /// Check the health of the system and return the components and their individual health
    pub fn check_health(&self) -> HashMap<String, Result<String, String>> {
        let mut result = HashMap::new();
        for (k, v) in self.components.iter() {
            let component_health = v.check_health();
            match &component_health {
                Ok(msg) => {
                    debug!("Component {} is healthy: {}", k, msg);
                }
                Err(msg) => {
                    error!("Component {} is unhealthy: {}", k, msg);
                }
            };
            result.insert(k.clone(), component_health);
        }
        result
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::health::Healthcheck;
    use galvanic_assert::{*, matchers::*};

    #[test]
    fn test_no_checks() {
        let checks = HashMap::new();
        let healthchecker = Healthchecker::new(checks);

        let results = healthchecker.check_health();

        let expected = HashMap::new();
        assert_that!(&results, eq(expected));
    }

    #[test]
    fn test_passing_check() {
        let mut checks: HashMap<String, Arc<dyn Healthcheck>> = HashMap::new();
        checks.insert("passing".to_owned(), Arc::new(Ok("Check Passed")));
        let healthchecker = Healthchecker::new(checks);

        let results = healthchecker.check_health();

        let mut expected = HashMap::new();
        expected.insert("passing".to_owned(), Ok("Check Passed".to_owned()));
        assert_that!(&results, eq(expected));
    }

    #[test]
    fn test_failing_check() {
        let mut checks: HashMap<String, Arc<dyn Healthcheck>> = HashMap::new();
        checks.insert("failing".to_owned(), Arc::new(Err("Check Failed")));
        let healthchecker = Healthchecker::new(checks);

        let results = healthchecker.check_health();

        let mut expected = HashMap::new();
        expected.insert("failing".to_owned(), Err("Check Failed".to_owned()));
        assert_that!(&results, eq(expected));
    }

    #[test]
    fn test_mixed_checks() {
        let mut checks: HashMap<String, Arc<dyn Healthcheck>> = HashMap::new();
        checks.insert("passing".to_owned(), Arc::new(Ok("Check Passed")));
        checks.insert("failing".to_owned(), Arc::new(Err("Check Failed")));
        let healthchecker = Healthchecker::new(checks);

        let results = healthchecker.check_health();

        let mut expected = HashMap::new();
        expected.insert("failing".to_owned(), Err("Check Failed".to_owned()));
        expected.insert("passing".to_owned(), Ok("Check Passed".to_owned()));
        assert_that!(&results, eq(expected));
    }
}
