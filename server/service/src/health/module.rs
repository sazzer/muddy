use super::http;
use super::Healthcheck;
use super::Healthchecker;
use crate::web::WebConfigurable;
use rocket::{routes, Rocket};
use std::collections::HashMap;
use std::sync::Arc;

/// Module for managing the healthchecks
pub struct HealthModule {
    healthchecker: Arc<Healthchecker>,
}

impl HealthModule {
    /// Construct a new instance of the Health Module
    pub fn new(components: HashMap<String, Arc<dyn Healthcheck>>) -> Self {
        let healthchecker = Healthchecker::new(components);

        HealthModule {
            healthchecker: Arc::new(healthchecker),
        }
    }
}

impl WebConfigurable for HealthModule {
    /// Configure the web server to provide the healthcheck routes
    fn configure_web_server(&self, rocket: Rocket) -> Rocket {
        rocket
            .manage(self.healthchecker.clone())
            .mount("/", routes![http::health])
    }
}
