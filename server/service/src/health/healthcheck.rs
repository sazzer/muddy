/// Trait for any component that can report it's health
pub trait Healthcheck: Send + Sync {
    /// Check the health of the component.
    /// This returns a `Result<String, String>` which is either `Ok(msg)` for a healthy
    /// component, or `Err(msg)` for an unhealthy component.
    /// In both cases, the `Result<>` contains a message representing the state of the component
    fn check_health(&self) -> Result<String, String>;
}

#[cfg(test)]
impl Healthcheck for Result<&str, &str> {
    fn check_health(&self) -> Result<String, String> {
        match *self {
            Ok(msg) => Ok(msg.to_owned()),
            Err(msg) => Err(msg.to_owned()),
        }
    }
}
