use super::model::*;
use crate::health::Healthchecker;
use rocket::{get, http::Status, response::status::Custom, State};
use rocket_contrib::json::Json;
use std::collections::HashMap;
use std::sync::Arc;

/// The actual handler that will perform the health checks
#[get("/health", format = "json")]
pub fn health(healthchecker: State<Arc<Healthchecker>>) -> Custom<Json<SystemHealth>> {
    let system_health = healthchecker.check_health();
    let mut components = HashMap::new();

    for (k, v) in system_health {
        let component_health = match v {
            Ok(msg) => ComponentHealth {
                healthy: true,
                message: msg,
            },
            Err(msg) => ComponentHealth {
                healthy: false,
                message: msg,
            },
        };
        components.insert(k.clone(), component_health);
    }

    let healthy = components.iter().all(|(_, v)| v.healthy);

    let health = SystemHealth {
        healthy,
        components,
    };

    let status_code = if healthy {
        Status::Ok
    } else {
        Status::InternalServerError
    };
    Custom(status_code, Json(health))
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::health::*;
    use galvanic_assert::{matchers::*, *};
    use rocket::{local::Client, routes};
    use serde_json::{from_str, json, Value};

    fn test_health_handler(
        checks: HashMap<String, Arc<dyn Healthcheck>>,
        expected_status_code: Status,
        expected_body: Value,
    ) {
        let healthchecker = Healthchecker::new(checks);

        let rocket = rocket::ignite()
            .manage(Arc::new(healthchecker))
            .mount("/", routes![health]);
        let client = Client::new(rocket).expect("valid rocket instance");
        let mut response = client.get("/health").dispatch();
        assert_that!(&response.status(), eq(expected_status_code));

        let actual_body: Value = from_str(&response.body_string().unwrap()).unwrap();
        assert_that!(&actual_body, eq(expected_body));
    }

    #[test]
    fn test_no_checks() {
        let checks: HashMap<String, Arc<dyn Healthcheck>> = HashMap::new();
        test_health_handler(
            checks,
            Status::Ok,
            json!({
                "healthy": true,
                "components": {}
            }),
        );
    }

    #[test]
    fn test_passing_checks() {
        let mut checks: HashMap<String, Arc<dyn Healthcheck>> = HashMap::new();
        checks.insert("passing".to_owned(), Arc::new(Ok("Check Passed")));
        test_health_handler(
            checks,
            Status::Ok,
            json!({
                "healthy": true,
                "components": {
                    "passing": {
                        "healthy": true,
                        "message": "Check Passed"
                    }
                }
            }),
        );
    }

    #[test]
    fn test_failing_checks() {
        let mut checks: HashMap<String, Arc<dyn Healthcheck>> = HashMap::new();
        checks.insert("failing".to_owned(), Arc::new(Err("Check Failed")));
        test_health_handler(
            checks,
            Status::InternalServerError,
            json!({
                "healthy": false,
                "components": {
                    "failing": {
                        "healthy": false,
                        "message": "Check Failed"
                    }
                }
            }),
        );
    }

    #[test]
    fn test_mixed_checks() {
        let mut checks: HashMap<String, Arc<dyn Healthcheck>> = HashMap::new();
        checks.insert("passing".to_owned(), Arc::new(Ok("Check Passed")));
        checks.insert("failing".to_owned(), Arc::new(Err("Check Failed")));
        test_health_handler(
            checks,
            Status::InternalServerError,
            json!({
                "healthy": false,
                "components": {
                    "passing": {
                        "healthy": true,
                        "message": "Check Passed"
                    },
                    "failing": {
                        "healthy": false,
                        "message": "Check Failed"
                    }
                }
            }),
        );
    }
}
