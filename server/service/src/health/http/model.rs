use serde::Serialize;
use std::collections::HashMap;

/// API Representation of the health of a single component
#[derive(Serialize)]
pub struct ComponentHealth {
    pub healthy: bool,
    pub message: String,
}

/// API Representation of the health of the whole system
#[derive(Serialize)]
pub struct SystemHealth {
    pub healthy: bool,
    pub components: HashMap<String, ComponentHealth>,
}
