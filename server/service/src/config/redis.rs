use envconfig::*;
use envconfig_derive::*;

/// Configuration for the Redis module
#[derive(Envconfig, Debug)]
pub struct Config {
    /// The URL to connect to
    #[envconfig(from = "REDIS_URL")]
    pub url: String,
}
