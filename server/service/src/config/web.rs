use envconfig::*;
use envconfig_derive::*;

/// Configuration for the Web module
#[derive(Envconfig, Debug)]
pub struct Config {
    /// The port to listen on
    #[envconfig(from = "PORT", default = "8000")]
    pub port: u16,
}
