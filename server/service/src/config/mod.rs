pub mod database;
pub mod redis;
pub mod web;

use envconfig::Envconfig;

/// Wrapper around all the application config
#[derive(Debug)]
pub struct Config {
    pub database: database::Config,
    pub redis: redis::Config,
    pub web: web::Config,
}

impl Config {
    /// Construct a new instance of the configuration struct
    pub fn new() -> Result<Config, envconfig::Error> {
        Ok(Config {
            database: database::Config::init()?,
            redis: redis::Config::init()?,
            web: web::Config::init()?,
        })
    }
}
