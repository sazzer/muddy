use envconfig::*;
use envconfig_derive::*;

/// Configuration for the Database module
#[derive(Envconfig, Debug)]
pub struct Config {
    /// The URL to connect to
    #[envconfig(from = "DATABASE_URL")]
    pub url: String,
}
