#[macro_use]
extern crate log;

fn main() {
    // We can safely ignore if the .env file is missing
    let _ = dotenv::dotenv();
    env_logger::init();

    let config = muddy_server::config::Config::new().unwrap();
    let server = muddy_server::build_server(config);

    info!("Starting server...");
    server.launch();
}
