#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate log;

pub mod config;
mod database;
mod health;
mod http;
mod model;
mod races;
mod redis;
mod web;

use health::Healthcheck;
use std::collections::HashMap;
use std::sync::Arc;

/// Build the server structure
pub fn build_server(config: config::Config) -> rocket::Rocket {
    debug!("Building server with config: {:?}", config);

    // Set up the data stores
    let redis_module =
        redis::RedisModule::new(&config.redis.url).expect("Redis Module Instantiation");
    let database_module =
        database::DatabaseModule::new(&config.database.url).expect("Database Module Instantiation");

    // Set up the actual functional modules
    let races_module = races::RacesModule::new(&database_module);

    // Create the healthchecks
    let mut health_checkers: HashMap<String, Arc<dyn Healthcheck>> = HashMap::new();
    health_checkers.insert("redis".to_owned(), redis_module.get_redis_wrapper());
    health_checkers.insert(
        "database".to_owned(),
        database_module.get_database_wrapper(),
    );
    let health_module = health::HealthModule::new(health_checkers);

    // And then the web server
    let web_module = web::WebModule::new(config.web.port, vec![&health_module, &races_module]);
    web_module.build_server().expect("Web Module Instantiation")
}
