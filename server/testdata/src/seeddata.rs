use postgres::types::ToSql;

/// SeedData is a trait that represents some data that can be seeded into the database
pub trait SeedData {
    /// The query that is used to insert the data into the database
    fn query(&self) -> String;

    /// The binds that are used when inserting the data into the database
    fn binds(&self) -> Vec<&dyn ToSql>;
}
