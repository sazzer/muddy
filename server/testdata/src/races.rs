use crate::seeddata::SeedData;
use chrono::{DateTime, Utc};
use postgres::types::ToSql;
use uuid::Uuid;

/// A race that can be seeded into the database
pub struct RaceSeed {
    pub id: Uuid,
    pub version: Uuid,
    pub created: DateTime<Utc>,
    pub updated: DateTime<Utc>,
    pub name: String,
    pub description: String,
}

impl SeedData for RaceSeed {
    /// The query that is used to insert the data into the database
    fn query(&self) -> String {
        "INSERT INTO races (race_id, version, created, updated, name, description) VALUES ($1, $2, $3, $4, $5, $6)".to_owned()
    }

    /// The binds that are used when inserting the data into the database
    fn binds(&self) -> Vec<&dyn ToSql> {
        vec![
            &self.id,
            &self.version,
            &self.created,
            &self.updated,
            &self.name,
            &self.description,
        ]
    }
}

impl Default for RaceSeed {
    fn default() -> Self {
        RaceSeed {
            id: Uuid::new_v4(),
            version: Uuid::new_v4(),
            created: DateTime::parse_from_rfc3339("1996-12-19T16:39:57Z")
                .unwrap()
                .with_timezone(&Utc),
            updated: DateTime::parse_from_rfc3339("1996-12-24T16:39:57Z")
                .unwrap()
                .with_timezone(&Utc),
            name: "Test Race".to_owned(),
            description: "This is a test race".to_owned(),
        }
    }
}
